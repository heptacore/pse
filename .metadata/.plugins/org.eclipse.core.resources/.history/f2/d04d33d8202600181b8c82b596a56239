package utilidades;

import java.util.ArrayList;

/**
 * Esta interfaz define los m�todos escenciales para la gesti�n
 * de las entidades persistentes del sistema y la relaci�n de estas
 * con la clase de conexi�n.
 * @author Oliver
 * @see Conexion
 * @version 1.0
 */
public interface Entidad {
	/**
	 * Devuelve todos los registros de la base de datos con el tipo de dato
	 * especificado contenidos en un ArrayList.
	 * @author Oliver
	 * @version 1.0
	 * @param entityName el nombre de la tabla en el esquema persistente.
	 * @param entityPOJO tipo de dato al cual se convertir�n los resultados de la operaci�n.
	 * @return
	 */
	public ArrayList<?> retrieveData(String entityName, Object entityPOJO);

	/**
	 * Crea registros persistentes en la base de datos asociada al sistema en
	 * la tabla especificada con los datos de los objetos pasados por par�metros.
	 * @param entityName nombre de la tabla en el esquema persistente.
	 * @param entityPOJO conjunto de objetos a ser guardados en la base de datos.
	 * @return 0 = �xito, 1 = fallo
	 */
	public int saveData(String entityName, ArrayList<?> entityPOJOArray);

	/**
	 * Devuelve un conjunto de registros del esquema de base de datos
	 * con un tipo de dato especificado mediante un objeto POJO y encapsulados
	 * o contenidos en un objeto del tipo ArrayList.
	 * @param filter sujeto de b�squeda.
	 * @param argument argumento de b�squeda.
	 * @param entityPOJO tipo de dato de conversi�n final.
	 * @return
	 */
	public ArrayList<?> findData(String argument);
}
