package data;

import java.util.ArrayList;
import model.Student;
import utilidades.Connection;
import utilidades.Persistency;
import model.Phone;
import model.Email;
import model.Entity;
import java.sql.SQLException;

public class Student_Data implements Persistency {
	//	Attributes
	public static Student_Data instance;
	private java.sql.ResultSet resultSet = null;
	private ArrayList<Student> students = new ArrayList<Student>();
	private ArrayList<Phone> phones = new ArrayList<Phone>();
	private ArrayList<Email> emailAddresses = new ArrayList<Email>();
	
	private Student_Data(){}
	
	public static Student_Data getInstance() {
		if (instance == null) {	//	There are no instances of the class.
			instance = new Student_Data();
		}
		return instance;
	}

	public ArrayList<Student> getStudents() {
		return students;
	}
	
	/**
	 * @deprecated
	 * @param index
	 * @return
	 */
	private String getQuery(int index) {
		String query = null;
		switch (index) {
		case 0:	//	Get all rows.
			query = "SELECT * FROM Persona;";
			break;
		case 1:	//	Get all phones from a student.
			query = "SELECT * FROM Telefono WHERE Telefono.propietario = ";
			break;
		default:
			query = "";
			break;
		}
		return query;
	}
	
	@Override
	public int createData(ArrayList<?> objects) {
		//	TODO	Create method.
		return 0;
	}
	
	@Override
	public ArrayList<Student> readData() throws SQLException {
		//	TODO	Complete me!
		this.students.clear();
		this.resultSet = Connection.doQuery("SELECT * FROM Persona;");
		while (resultSet.next()) {
			Student newStudent = new Student();
			newStudent.setIdPerson(resultSet.getInt("idPersona"));
			newStudent.setFirstName(resultSet.getString("primerNombre"));
			students.add(newStudent);
		}
		Connection.endQuery();
		
		for (int i = 0; i < students.size(); i++) {
			this.resultSet = Connection.doQuery("SELECT * FROM Telefono WHERE propietario = "+students.get(i).getIdPerson()+";");
			while (resultSet.next()){
				Phone newPhone = new Phone(resultSet.getInt("idTelefono"),
						resultSet.getString("numero"),
						resultSet.getString("etiqueta"));
				System.out.println(newPhone.getNumber());
				phones.add(newPhone);
			}
			students.get(i).setPhones(phones);
			System.out.println(students.get(i).getFirstName()+", "+students.get(i).getPhones().get(0).getNumber());
			phones.clear();
		}
		return students;
	}
	
	@Override
	public int updateData(Entity object) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteData(Entity object) {
		// TODO Auto-generated method stub
		return 0;
	}
}
