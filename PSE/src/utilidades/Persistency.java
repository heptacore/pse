package utilidades;

import java.sql.SQLException;
import java.util.ArrayList;
import model.Entity;

public interface Persistency {
	public int createData(ArrayList<?> objects);
	
	public ArrayList<?> readData() throws SQLException;

	public int updateData(Entity object);
	
	public int deleteData(Entity object);
}
