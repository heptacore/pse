package model;

public class Phone implements Entity {
	
	//	Attributes
	private int id;
	private String number;
	private String tag;
	
	//	Methods
	public Phone(){}
	
	public Phone(int id, String number, String tag) {
		this.id = id;
		this.number = number;
		this.tag = tag;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
}
