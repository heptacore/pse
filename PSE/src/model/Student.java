package model;

public class Student extends Person implements Entity {
	//	Attributes
	private int idStudent;	//	Unique DataBase value.
	private String studentIdNumber;
	private String exStudentIdNumber;
	private String cohort;
	
	//	Methods
	public Student(){}
	
	public Student(int idStudent,
			String studentIdNumber,
			String exStudentIdNumber,
			String cohort,
			int idPerson,
			String idCardNumber,
			String firstName,
			String lastName,
			String surname,
			String secondSurname,
			char gender){
		this.idStudent = idStudent;
		this.studentIdNumber = studentIdNumber;
		this.exStudentIdNumber = exStudentIdNumber;
		this.cohort = cohort;
		this.idPerson = idPerson;
		this.idCardNumber = idCardNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.surname = surname;
		this.secondSurname = secondSurname;
		this.gender = gender;
	}
	
	public int getId() {
		return idStudent;
	}
	public void setId(int id) {
		this.idStudent = id;
	}
	public String getStudentIdNumber() {
		return studentIdNumber;
	}
	public void setStudentIdNumber(String studentIdNumber) {
		this.studentIdNumber = studentIdNumber;
	}
	public String getExStudentIdNumber() {
		return exStudentIdNumber;
	}
	public void setExStudentIdNumber(String exStudentIdNumber) {
		this.exStudentIdNumber = exStudentIdNumber;
	}
	public String getCohort() {
		return cohort;
	}
	public void setCohort(String cohort) {
		this.cohort = cohort;
	}	
}