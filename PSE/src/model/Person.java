package model;

import java.util.ArrayList;

/**
 * This class represents a Person in the ERD Diagram.
 * It's methods are not abstract for they define an implementation
 * which is not going to be overwritten.
 * @author Oliver
 * @version 1.0
 */
public class Person implements Entity {
	//	Attributes
	protected int idPerson;
	protected String idCardNumber;
	protected String firstName;
	protected String lastName;
	protected String surname;
	protected String secondSurname;
	protected char gender;
	
	/**
	 * Attribute included as a result of the composite relationship
	 * between this class and Phone class, where this class
	 * is the whole and Phone is the part.
	 * @see Phone
	 */
	protected ArrayList<Phone> phones;
	
	/**
	 * Attribute included as a result of the composite relationship
	 * between this class and Email class, where this class
	 * is the whole and Email is the part.
	 */
	protected ArrayList<Email> emailAddresses;

	//	Methods
	/**
	 * Builds a Person-Type object with empty attributes.
	 */
	public Person() {}
	
	/**
	 * Builds a Person-Type object with all its attributes initialized.
	 * @param id Database identifier value.
	 * @param idCardNumber
	 * @param firstName
	 * @param lastName
	 * @param surname
	 * @param secondSurname
	 * @param gender
	 */
	public Person(int id, String idCardNumber, String firstName, String lastName, String surname, String secondSurname, char gender) {
		this.idPerson = id;
		this.idCardNumber = idCardNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.surname = surname;
		this.secondSurname = secondSurname;
		this.gender = gender;
	}
	
	/**
	 * Build a Person-Type object with all its attributes initialized but "gener" wich can
	 * be NULL in DataBase Schema.
	 * @param id DataBase unique value.
	 * @param idCardNumber
	 * @param firstName
	 * @param lastName
	 * @param surname
	 * @param secondSurname
	 */
	public Person(int id, String idCardNumber, String firstName, String lastName, String surname, String secondSurname) {
		this.idPerson = id;
		this.idCardNumber = idCardNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.surname = surname;
		this.secondSurname = secondSurname;
	}

	public int getIdPerson() {
		return idPerson;
	}

	public void setIdPerson(int id) {
		this.idPerson = id;
	}

	public String getIdCardNumber() {
		return idCardNumber;
	}

	public void setIdCardNumber(String idCardNumber) {
		this.idCardNumber = idCardNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSecondSurname() {
		return secondSurname;
	}

	public void setSecondSurname(String secondSurname) {
		this.secondSurname = secondSurname;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public ArrayList<Phone> getPhones() {
		return phones;
	}

	public void setPhones(ArrayList<Phone> phones) {
		this.phones = phones;
	}

	public ArrayList<Email> getEmailAddresses() {
		return emailAddresses;
	}

	public void setEmailAddresses(ArrayList<Email> emailAddresses) {
		this.emailAddresses = emailAddresses;
	}	
}